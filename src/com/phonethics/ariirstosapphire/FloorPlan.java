package com.phonethics.ariirstosapphire;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class FloorPlan extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan);
        
        TouchImageView locationImg = (TouchImageView) findViewById(R.id.image_FloorPlan);
        locationImg.setMaxZoom(3f);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_floor_plan, menu);
        return false;
    }
    
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
}
