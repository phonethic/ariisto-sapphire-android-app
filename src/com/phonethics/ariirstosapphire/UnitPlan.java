package com.phonethics.ariirstosapphire;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class UnitPlan extends Activity {

	LinearLayout				layout;
	TextView[]      			text;
	Context						context;
	ToggleButton				toggle;
	ViewPagerAdapter 			adapter;
	Typeface 					tf;
	ImageView					info_image, img_internalInfo;
	int							position;
	TextView					bhk_txt;
	ArrayList<Integer> 			bhk2_imgs;
	ArrayList<Integer> 			bhk3_imgs;
	ArrayList<String> 			bhkInfoText;
	ViewPager 					myPager;
	Animation					animSlideIn, animSlideOut;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_unit_plan);

		context 			= this;
		layout 				= (LinearLayout) findViewById(R.id.linear_text);
		toggle 				= (ToggleButton) findViewById(R.id.toggle_unit);
		tf 					= Typeface.createFromAsset(getResources().getAssets(),"fonts/bookman.ttf");
		info_image			= (ImageView) findViewById(R.id.image_i);
		img_internalInfo 	= (ImageView) findViewById(R.id.img_internalInfo);
		bhk_txt			= (TextView) findViewById(R.id.text_bhkInfo);
		myPager 			= (ViewPager) findViewById(R.id.myfivepanelpager);
		animSlideIn			= AnimationUtils.loadAnimation(context, R.anim.slide_in_up);
		animSlideOut			= AnimationUtils.loadAnimation(context, R.anim.slide_out_down);

		bhk2_imgs = new ArrayList<Integer>();
		bhk3_imgs = new ArrayList<Integer>();
		bhkInfoText = new ArrayList<String>();

		bhk2_imgs.add(R.drawable.bhk2_2d);
		bhk2_imgs.add(R.drawable.bhk2_3d);

		bhk3_imgs.add(R.drawable.bhk3_2d);
		bhk3_imgs.add(R.drawable.bhk3_3d);

		bhkInfoText.add("2 BEDROOM APARTMENT\n\n-Total 83 Flats -39 Flats of 2BHK.\n-'A' Wing -19 Units * 'B' Wing - 20 Units\n-Area of each flat - 1285 sq.ft.");
		bhkInfoText.add("3 BEDROOM APARTMENT\n\n-Total 83 Flats -44 Flats of 3BHK.\n-'A' Wing -22 Units * 'B' Wing - 22 Units\n-Area of each flat - 1860 sq.ft.");

		adapter = new ViewPagerAdapter(context, bhk2_imgs);
		text = new TextView[bhk2_imgs.size()];
		for(int i = 0; i <bhk2_imgs.size() ; i++){
			text[i] = new TextView(context);
			text[i].setTextSize(45f);
			text[i].setTextColor(Color.GRAY);
			text[i].setText(".");
			text[i].setTypeface(tf);

			layout.addView(text[i]);	
		}
		text[0].setTextColor(Color.WHITE);
		myPager.setAdapter(adapter);
		myPager.setCurrentItem(0);

		toggle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(toggle.isChecked() ){
					adapter = new ViewPagerAdapter(context, bhk3_imgs);
					text = new TextView[bhk3_imgs.size()];
					layout.removeAllViews();
					for(int i = 0; i <bhk3_imgs.size() ; i++){
						text[i] = new TextView(context);
						text[i].setTextSize(45f);
						text[i].setTextColor(Color.GRAY);
						text[i].setText(".");
						text[i].setTypeface(tf);
						layout.addView(text[i]);	
					}
					text[0].setTextColor(Color.WHITE);
					
					bhk_txt.setText(bhkInfoText.get(1));

					myPager.setAdapter(adapter);
					myPager.setCurrentItem(0);


				}else{
					adapter = null;
					adapter = new ViewPagerAdapter(context, bhk2_imgs);
					text = new TextView[bhk2_imgs.size()];
					layout.removeAllViews();
					for(int i = 0; i <bhk2_imgs.size() ; i++){
						text[i] = new TextView(context);
						text[i].setTextSize(45f);
						text[i].setTextColor(Color.GRAY);
						text[i].setText(".");
						text[i].setTypeface(tf);
						layout.addView(text[i]);	
					}
					text[0].setTextColor(Color.WHITE);
					bhk_txt.setText(bhkInfoText.get(0));

					myPager.setAdapter(adapter);
					myPager.setCurrentItem(0);

					if(bhk_txt.isShown()){
						if(toggle.isChecked()){
							/*Toast.makeText(context, "External Amentites", Toast.LENGTH_SHORT).show();*/
							bhk_txt.setText(bhkInfoText.get(1));
						}else{
							bhk_txt.setText(bhkInfoText.get(0));
						}
					}

				}

			}

		});

		myPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				info_image.setVisibility(View.VISIBLE);
				position = arg0;
				for(int i=0;i<text.length;i++)
				{
					if(i==arg0)
					{
						text[i].setTextColor(Color.WHITE);
					}
					else
					{
						text[i].setTextColor(Color.GRAY);
					}
				}



			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				info_image.setVisibility(View.GONE);
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				info_image.setVisibility(View.VISIBLE);
			}
		});

		info_image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*if(toggle.isChecked()){
					Toast.makeText(context, "External Amentites", Toast.LENGTH_SHORT).show();
					if(bhk_txt.isShown()){
						bhk_txt.setVisibility(View.GONE);
					}else{
						bhk_txt.setVisibility(View.VISIBLE);
						bhk_txt.setText(bhkInfoText.get(1));
					}
				}else{
					bhk_txt.setVisibility(View.VISIBLE);
					bhk_txt.setText(bhkInfoText.get(0));
				}
				*/
				if(bhk_txt.isShown()){
					bhk_txt.startAnimation(animSlideOut);
					bhk_txt.setVisibility(View.GONE);
					
				}else{
					bhk_txt.startAnimation(animSlideIn);
					bhk_txt.setVisibility(View.VISIBLE);
					if(toggle.isChecked()){
						bhk_txt.setText(bhkInfoText.get(1));
					}else{
						bhk_txt.setText(bhkInfoText.get(0));
					}
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_unit_plan, menu);
		return false;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}


	public class ViewPagerAdapter extends PagerAdapter{


		Context activity;
		ArrayList<Integer> imageArray;

		public ViewPagerAdapter(Context context, ArrayList<Integer> imgArra) {
			imageArray 	= imgArra;
			activity 	= context;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			// TODO Auto-generated method stub
			ImageView view = new ImageView(activity);
			/*	view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));*/
			view.setScaleType(ScaleType.FIT_XY);
			view.setBackgroundResource(imageArray.get(position));
			((ViewPager) container).addView(view, 0);
			return view;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imageArray.size();
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == ((View) arg1);
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

	}
}
