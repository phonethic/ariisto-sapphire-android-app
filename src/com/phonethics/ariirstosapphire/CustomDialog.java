package com.phonethics.ariirstosapphire;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class CustomDialog extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_dialog);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_custom_dialog, menu);
        return true;
    }
}
