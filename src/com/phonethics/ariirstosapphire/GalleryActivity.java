package com.phonethics.ariirstosapphire;

import java.util.ArrayList;

import com.phonethics.ariirstosapphire.BrandPatners.ViewPagerAdapter;

import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class GalleryActivity extends Activity {

	LinearLayout				layout;
	TextView[]      			text;
	Context						context;
	ViewPagerAdapter 			adapter;
	Typeface 					tf;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        
        context 	= this;
		layout 		= (LinearLayout) findViewById(R.id.linear_text_gallery);
		tf 			= Typeface.createFromAsset(getResources().getAssets(),"fonts/bookman.ttf");
		final ArrayList<Integer> gallery_imgs = new ArrayList<Integer>();
		
		gallery_imgs.add(R.drawable.kitchen);
		gallery_imgs.add(R.drawable.livingroom);
		gallery_imgs.add(R.drawable.bedroom);
		gallery_imgs.add(R.drawable.bathroom);
		
		gallery_imgs.add(R.drawable.yoga);
		gallery_imgs.add(R.drawable.swimmingpool);
		gallery_imgs.add(R.drawable.steamroom);
		gallery_imgs.add(R.drawable.lobby);
		gallery_imgs.add(R.drawable.gym);
		gallery_imgs.add(R.drawable.garden);
		gallery_imgs.add(R.drawable.chess);
		gallery_imgs.add(R.drawable.billiards);
		gallery_imgs.add(R.drawable.basketball);
		
		final ViewPager myPager = (ViewPager) findViewById(R.id.myfivepanelpager);
		adapter = new ViewPagerAdapter(context, gallery_imgs);
		text = new TextView[gallery_imgs.size()];
		for(int i = 0; i <gallery_imgs.size() ; i++){
			text[i] = new TextView(context);
			text[i].setTextSize(45f);
			text[i].setTextColor(Color.GRAY);
			text[i].setText(".");
			text[i].setTypeface(tf);

			layout.addView(text[i]);	
		}
		text[0].setTextColor(Color.WHITE);
		myPager.setAdapter(adapter);
		myPager.setCurrentItem(0);

		
		myPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				
				for(int i=0;i<text.length;i++)
				{
					if(i==arg0)
					{
						text[i].setTextColor(Color.WHITE);
					}
					else
					{
						text[i].setTextColor(Color.GRAY);
					}
				}

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
		
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
			
			}
		});
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_brand_patners, menu);
        return false;
    }
    
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
    
    public class ViewPagerAdapter extends PagerAdapter{


		Context activity;
		ArrayList<Integer> imageArray;

		public ViewPagerAdapter(Context context, ArrayList<Integer> imgArra) {
			imageArray 	= imgArra;
			activity 	= context;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			// TODO Auto-generated method stub
			ImageView view = new ImageView(activity);
			/*	view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));*/
			view.setScaleType(ScaleType.FIT_XY);
			view.setBackgroundResource(imageArray.get(position));
			((ViewPager) container).addView(view, 0);
			return view;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imageArray.size();
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == ((View) arg1);
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

	}
}
