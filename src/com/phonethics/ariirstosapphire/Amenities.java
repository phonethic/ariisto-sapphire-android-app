package com.phonethics.ariirstosapphire;



import java.util.ArrayList;

import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewSwitcher;
import android.widget.ImageView.ScaleType;

public class Amenities extends Activity {

	LinearLayout				layout;
	TextView[]      			text;
	Context						context;
	ToggleButton				toggle;
	ViewPagerAdapter 			adapter;
	Typeface 					tf;
	ImageView					info_image, img_internalInfo;
	int							position;
	ViewSwitcher				viewPager;
	RelativeLayout				rellayout;
	TextView					external_txt;
	Animation					animSlideIn, animSlideOut,slideOutLeft,sledeInRight;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_amenities);

		context 	= this;
		layout 		= (LinearLayout) findViewById(R.id.linear_text);
		toggle 		= (ToggleButton) findViewById(R.id.toggleAmenities);
		tf 			= Typeface.createFromAsset(getResources().getAssets(),"fonts/bookman.ttf");
		
		info_image			= (ImageView) findViewById(R.id.image_i);
		img_internalInfo 	= (ImageView) findViewById(R.id.img_internalInfo);
		
		viewPager		= (ViewSwitcher) findViewById(R.id.viewpage);
		rellayout		= (RelativeLayout) findViewById(R.id.second_layout);
		external_txt	= (TextView) findViewById(R.id.text_externalInfo);
		
		animSlideIn				= AnimationUtils.loadAnimation(context, R.anim.slide_in_up);
		animSlideOut			= AnimationUtils.loadAnimation(context, R.anim.slide_out_down);
		slideOutLeft			= AnimationUtils.loadAnimation(context, R.anim.slide_out_right);

		final ArrayList<Integer> external_imgs = new ArrayList<Integer>();
		final ArrayList<Integer> internal_imgs = new ArrayList<Integer>();
		final ArrayList<Integer> internalInfo_imgs = new ArrayList<Integer>();

		external_imgs.add(R.drawable.yoga);
		external_imgs.add(R.drawable.swimmingpool);
		external_imgs.add(R.drawable.steamroom);
		external_imgs.add(R.drawable.lobby);
		external_imgs.add(R.drawable.gym);
		external_imgs.add(R.drawable.garden);
		external_imgs.add(R.drawable.chess);
		external_imgs.add(R.drawable.billiards);
		external_imgs.add(R.drawable.basketball);

		internal_imgs.add(R.drawable.kitchen);
		internal_imgs.add(R.drawable.livingroom);
		internal_imgs.add(R.drawable.bedroom);
		internal_imgs.add(R.drawable.bathroom);

		internalInfo_imgs.add(R.drawable.moreconvenience);
		internalInfo_imgs.add(R.drawable.moreplush);
		internalInfo_imgs.add(R.drawable.moresophistication);
		internalInfo_imgs.add(R.drawable.morelifestyle);

		/*R.drawable.swimmingpool,
				R.drawable.steamroom, 
				R.drawable.lobby,
				R.drawable.gym, 
				R.drawable.garden,
				R.drawable.chess, 
				R.drawable.billiards,
				R.drawable.basketball};*/

		/*final int imageArra1[] = { R.drawable.kitchen,
				R.drawable.livingroom,
				R.drawable.bedroom,
				R.drawable.bathroom, 
		};*/



		/*final int internalInfoArr[] = { R.drawable.moreconvenience,
				R.drawable.moreplush,
				R.drawable.moresophistication,
				R.drawable.morelifestyle, 
		};*/

		final String external_infoArr[] = {
				"YOGA ROOM\nRelease the stress of mind and body with a serene session at the yoga room.",
				"SWIMMING POOL\nEnjoy a splash with your children or cool downand recharge yourself with a few invigorating laps at the pool",
				"STEAM ROOM\nRelease the stress of day and body with a relaxing session at the steam room.",
				"ENTRANCE LOBBY",
				"GYMNASIUM\nMaintain your stamina and fitness levels with a regular dose of exercise while you are surrounded with a music and the latest LCD screens in the luxurious, well appointed air conditioned gymnasium",
				"LUSH GREEN GARDEN\nStrike stimulating conversation with like minded neighbours people on the same wavelength as yours.",
				"CLUB HOUSE\nStretch the limits of your mind and body as you challenge your family and friends to a quick game of carrom, chess, table tennis or cards.",
				"BILLIARDS\nYour cue to success follows you at home as you pocket a few balls over a casual game of billiards",
				"BASKET BALL COURT\nBreak into a quick sweat with a revitalizing game of basketball"
		};


		final ViewPager myPager = (ViewPager) findViewById(R.id.myfivepanelpager);
		adapter = new ViewPagerAdapter(context, internal_imgs);
		text = new TextView[internal_imgs.size()];
		for(int i = 0; i <internal_imgs.size() ; i++){
			text[i] = new TextView(context);
			text[i].setTextSize(45f);
			text[i].setTextColor(Color.GRAY);
			text[i].setText(".");
			text[i].setTypeface(tf);

			layout.addView(text[i]);	
		}
		text[0].setTextColor(Color.WHITE);
		myPager.setAdapter(adapter);
		myPager.setCurrentItem(0);

		toggle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(toggle.isChecked() ){
					adapter = new ViewPagerAdapter(context, external_imgs);
					text = new TextView[external_imgs.size()];
					layout.removeAllViews();
					for(int i = 0; i <external_imgs.size() ; i++){
						text[i] = new TextView(context);
						text[i].setTextSize(45f);
						text[i].setTextColor(Color.GRAY);
						text[i].setText(".");
						text[i].setTypeface(tf);
						layout.addView(text[i]);	
					}
					text[0].setTextColor(Color.WHITE);
					myPager.setAdapter(adapter);
					myPager.setCurrentItem(0);


				}else{
					adapter = null;
					adapter = new ViewPagerAdapter(context, internal_imgs);
					text = new TextView[internal_imgs.size()];
					layout.removeAllViews();
					for(int i = 0; i <internal_imgs.size() ; i++){
						text[i] = new TextView(context);
						text[i].setTextSize(45f);
						text[i].setTextColor(Color.GRAY);
						text[i].setText(".");
						text[i].setTypeface(tf);
						layout.addView(text[i]);	
					}
					text[0].setTextColor(Color.WHITE);


					myPager.setAdapter(adapter);
					myPager.setCurrentItem(0);
					if(external_txt.isShown()){
						external_txt.startAnimation(animSlideOut);
						external_txt.setVisibility(View.GONE);
					}
				}

			}

		});
		myPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				info_image.setVisibility(View.VISIBLE);
				position = arg0;
				for(int i=0;i<text.length;i++)
				{
					if(i==arg0)
					{
						text[i].setTextColor(Color.WHITE);
					}
					else
					{
						text[i].setTextColor(Color.GRAY);
					}
				}

				if(toggle.isChecked()){
					/*Toast.makeText(context, "External Amentites", Toast.LENGTH_SHORT).show();*/


					external_txt.setText(external_infoArr[position]);

				}


			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				info_image.setVisibility(View.GONE);
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				info_image.setVisibility(View.VISIBLE);
			}
		});

		info_image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(toggle.isChecked()){
					/*Toast.makeText(context, "External Amentites", Toast.LENGTH_SHORT).show();*/
					if(external_txt.isShown()){
						external_txt.startAnimation(animSlideOut);
						external_txt.setVisibility(View.GONE);
					}else{
						external_txt.startAnimation(animSlideIn);
						external_txt.setVisibility(View.VISIBLE);
						external_txt.setText(external_infoArr[position]);
					}
				}else{
					/*viewPager.startAnimation(slideOutLeft);*/
					viewPager.showNext();
					img_internalInfo.setBackgroundResource(internalInfo_imgs.get(position));
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_amenities, menu);
		return false;
	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(rellayout.isShown()){
			viewPager.showPrevious();
		}else{
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			this.finish();
		}

	}



	public class ViewPagerAdapter extends PagerAdapter{


		Context activity;
		ArrayList<Integer> imageArray;

		public ViewPagerAdapter(Context context, ArrayList<Integer> imgArra) {
			imageArray 	= imgArra;
			activity 	= context;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			// TODO Auto-generated method stub
			ImageView view = new ImageView(activity);
			/*	view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));*/
			view.setScaleType(ScaleType.FIT_XY);
			view.setBackgroundResource(imageArray.get(position));
			((ViewPager) container).addView(view, 0);
			return view;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imageArray.size();
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == ((View) arg1);
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

	}
}
