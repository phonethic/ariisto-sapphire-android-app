package com.phonethics.ariirstosapphire;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FeturesList extends Activity {

	ImageView 				imgFeture;
	ListView 				listview;
	ArrayList<String> 				values;
	CustomListAdapter 	adapter;
	public static boolean itemOnView = false;
	Context context;
	int tempPos;
	ArrayList<Boolean> saved = new ArrayList<Boolean>();
	int selected = 0;
	boolean itemSelected = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fetures_list);

		context = this;
		listview 	= (ListView) findViewById(R.id.listView1);
		values=new ArrayList<String>();
		values.add(" ESSENTIAL      ");
		values.add(" EMERGENCY     ");
		values.add(" ENTERTAINMENT  ");
		values.add(" VIDEO CALL     ");
		values.add(" DOCTORS     ");
		values.add(" SOCIETY      ");
		values.add(" FAMILY      ");
		values.add(" OTHERS      ");
		values.add(" WHAT'S NEW     ");

		imgFeture	= (ImageView) findViewById(R.id.imageFeatures);

		for(int i = 0; i<values.size();i++){
			saved.add(false);
		}
		adapter 	= new CustomListAdapter(this, values);
		listview.setAdapter(adapter);
		adapter.toggleSelected(0);
		/*listview.setSelection(0);*/



		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v,
					int position, long id) {
				imgFeture.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier("drawable/feature_"+position,"drawable",getPackageName())));
				adapter.toggleSelected(position);
				adapter.notifyDataSetChanged();

			}
		});

	}


	public class CustomListAdapter extends ArrayAdapter<String>
	{
		Activity context;
		ArrayList<String> row	=	null;
		LayoutInflater inflator	=	null;
		ArrayList<Boolean> isSelected = null;
		public ArrayList<Integer> selectedIds = new ArrayList<Integer>();


		public CustomListAdapter(Activity context,ArrayList<String> row)
		{
			super(context,R.layout.listrow,row);
			this.context	=	context;
			this.row		=	row;

		}


		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub 
			View rowView	=	convertView;
			final ViewHolder hold;

			if(convertView	==	null)
			{
				inflator					=	context.getLayoutInflater();
				final ViewHolder holder						=	new ViewHolder();
				rowView						=	inflator.inflate(R.layout.listrow, null);
				holder.rowText				=	(TextView) rowView.findViewById(R.id.rowListTextView);
				holder.layout 				=	(RelativeLayout) rowView.findViewById(R.id.text_relativelayout);
				

				rowView.setTag(R.id.rowListTextView, holder.rowText);
				rowView.setTag(R.id.text_relativelayout, holder.layout);
				rowView.setTag(holder);				

			}

			hold	=	(ViewHolder)rowView.getTag();
			hold.rowText.setText(row.get(position));
			
			  if (selectedIds.contains(position)){
				  hold.layout.setBackgroundColor(Color.WHITE);
				  hold.rowText.setTextColor(Color.parseColor("#a0468c"));
			  }else{
				  hold.layout.setBackgroundColor(Color.parseColor("#a0468c"));
				  hold.rowText.setTextColor(Color.WHITE);
			  }
			return rowView;
			
		}
		@Override
		public void notifyDataSetChanged() {
			// TODO Auto-generated method stub


			super.notifyDataSetChanged();
		}
		
		public void toggleSelected(Integer position)
		{

			selectedIds.clear();
			selectedIds.add(position);
		}
		
		public void setSelected(Integer position){
			
		}




	}

	static class ViewHolder
	{
		TextView rowText;
		RelativeLayout layout;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_fetures_list, menu);
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
}
